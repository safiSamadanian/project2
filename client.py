from __future__ import print_function
import logging
import grpc
import protoFile_pb2
import protoFile_pb2_grpc


def run():

    with grpc.insecure_channel('localhost:50051') as channel:
          
            stub = protoFile_pb2_grpc.ReplyStub(channel)
            print('Enter the name of the system whose information you want to receive:(sys1,sys2)')
            sysName=input()
            responseVersion = stub.ReturnVersion(protoFile_pb2.NameRequest(name=sysName))
            responseRenderer = stub.ReturnRenderer(protoFile_pb2.NameRequest(name=sysName))
            responseEnp3s0 = stub.ReturnEnp3s0(protoFile_pb2.NameRequest(name=sysName))
            print('Version: '+responseVersion.version)
            print('Renderer: '+responseRenderer.renderer)
            print('enp3s0: '+responseEnp3s0.enp3s0)
            
           

if __name__ == '__main__':
    logging.basicConfig()
    run()
