import json
import os
import redis
import ast
from concurrent import futures
import logging
import grpc
import protoFile_pb2
import protoFile_pb2_grpc

REDIS_HOST = os.getenv('SS_ABTEST_REDIS_HOST', '127.0.0.1')
r = redis.Redis(host=REDIS_HOST)

with open("JSONfile.json") as info:
    data = json.load(info)

# save data in redis
temp=[]
dataList=[]
for key, value in data.items():
    temp = [key,value]
    dataList.append(temp)
for i in range(0,len(dataList)) :
    r.set((dataList[i][0]),str(dataList[i][1]))


class Reply(protoFile_pb2_grpc.ReplyServicer):

    def ReturnVersion(self, request, context):
        dictFromRedis = ast.literal_eval(r.get(request.name).decode("UTF-8"))
        return protoFile_pb2.VersionReply( version= str(dictFromRedis['version']))


    def ReturnRenderer(self, request, context):
        dictFromRedis = ast.literal_eval(r.get(request.name).decode("UTF-8"))
        return protoFile_pb2.RendererReply( renderer=str(dictFromRedis['renderer']))

    def ReturnEnp3s0(self, request, context):
        dictFromRedis = ast.literal_eval(r.get(request.name).decode("UTF-8"))
        return protoFile_pb2.Enp3s0Reply(enp3s0=str(dictFromRedis['enp3s0']))

def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    protoFile_pb2_grpc.add_ReplyServicer_to_server(Reply(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    server.wait_for_termination()

if __name__ == '__main__':
    logging.basicConfig()
    serve()
